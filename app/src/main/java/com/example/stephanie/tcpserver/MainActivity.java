package com.example.stephanie.tcpserver;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Locale;


import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import static android.speech.RecognizerIntent.RESULT_NO_MATCH;

public class MainActivity extends AppCompatActivity {

    private ServerSocket serverSocket;

    Handler updateConversationHandler;

    Thread serverThread = null;

    private TextView text;

    public static final int SERVERPORT = 6000;

    private static int check = 111;

    String outcome ="";

    boolean gotSTT = false;
    TextToSpeech tts;
    AudioManager am;

    private SpeechRecognizer sr;
    private static final String TAG = "spk2txtD2";
    private TextView log;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        am.setStreamVolume(AudioManager.STREAM_MUSIC,am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),0);

        tts = new TextToSpeech(MainActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR){
                    tts.setLanguage(Locale.UK);
                }

            }
        });

        text = (TextView) findViewById(R.id.text2);
        log = (TextView) findViewById(R.id.textView);

        sr = SpeechRecognizer.createSpeechRecognizer(this);
        sr.setRecognitionListener(new listener());

        updateConversationHandler = new Handler();

        System.out.println("starting server");
        this.serverThread = new Thread(new ServerThread());
        this.serverThread.start();

    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class ServerThread implements Runnable {

        public void run() {
            Socket socket = null;
            try {
                serverSocket = new ServerSocket(SERVERPORT);
                System.out.println("server socket created");
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (!Thread.currentThread().isInterrupted()) {

                try {

                    System.out.println("accepting socket");
                    socket = serverSocket.accept();
                    System.out.println("accepted socket");
                    CommunicationThread commThread = new CommunicationThread(socket);
                    new Thread(commThread).start();
                    System.out.println("starting communication...");

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class CommunicationThread implements Runnable {

        private Socket clientSocket;

        private BufferedReader input;

        public CommunicationThread(Socket clientSocket) {

            this.clientSocket = clientSocket;

            try {

                this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void run() {

            OutputStream outputStream;

            while (!Thread.currentThread().isInterrupted()) {

                try {

                    final String read = input.readLine();
                    System.out.println("received: "+read);

                    if(read.equals("start")){

                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startListening();
                            }
                        });


                        gotSTT = true;
                    }else if (read.equals("VOLUMEON")){
                        System.out.println("setting volume to max");
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setVolume(false);
                            }
                        });
                        gotSTT = true;
                    }else if (read.equals("VOLUMEOFF")){
                        System.out.println("setting volume to mute");
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setVolume(true);
                            }
                        });
                        gotSTT = true;
                    }else{
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startTalking(read);
                            }
                        });
                        gotSTT = true;

                    }



                    System.out.println(outcome);

                    updateConversationHandler.post(new updateUIThread(read));

                    outputStream = clientSocket.getOutputStream();
                    PrintStream printStream = new PrintStream(outputStream);

                    System.out.println("gotSTT: "+gotSTT);

                    while(gotSTT){

                    }
                    printStream.print(outcome);
                    System.out.println("sent"+outcome);
                    printStream.close();
                    outcome = "";

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    class updateUIThread implements Runnable {
        private String msg;

        public updateUIThread(String str) {
            this.msg = str;
        }

        @Override
        public void run() {
            text.setText("Client Says: "+ msg + "\n");
        }
    }

    public void setVolume(boolean volume){
        if(volume){
            am.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
            System.out.println("muted");
            outcome = "Volume mute";
        }else if(!volume){
            am.setStreamVolume(AudioManager.STREAM_MUSIC, am.getStreamMaxVolume(am.STREAM_MUSIC), 0);
            System.out.println("un-muted");
            outcome = "Volume max";
        }
        gotSTT = false;
    }

    public void startListening(){
        Intent i =new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        i.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,getClass().getPackage().getName());
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        i.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        sr.startListening(i);
        //startActivityForResult(i,check);
        Log.i(TAG,"Intent sent");

    }

    public void startTalking(String speech){
        tts.speak(speech, TextToSpeech.QUEUE_FLUSH, null, null);
        while(tts.isSpeaking()){}
        outcome = "spoken";
        gotSTT =false;
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        System.out.println(RESULT_NO_MATCH +":"+RESULT_OK);

        if(requestCode == check && resultCode == RESULT_OK){
            ArrayList<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            for (String s : results)
            {
                outcome += s + "\t";
            }

            System.out.println(outcome);
        }
        else if(requestCode == check && resultCode == RESULT_NO_MATCH)
        {
            outcome = "DID NOT CATCH THAT";
            System.out.println(outcome);
        }

        gotSTT = false;
        super.onActivityResult(requestCode, resultCode, data);
    }

    class listener implements RecognitionListener {
        public void onReadyForSpeech(Bundle params)	{
            Log.d(TAG, "onReadyForSpeech");
        }
        public void onBeginningOfSpeech(){
            Log.d(TAG, "onBeginningOfSpeech");
        }
        public void onRmsChanged(float rmsdB){
            Log.d(TAG, "onRmsChanged");
        }
        public void onBufferReceived(byte[] buffer)	{
            Log.d(TAG, "onBufferReceived");
        }
        public void onEndOfSpeech()	{
            Log.d(TAG, "onEndofSpeech");
        }
        public void onError(int error)	{
            outcome = "DID NOT CATCH THAT";
            gotSTT = false;
            Log.d(TAG,  "error " +  error);
            logthis("error " + error);
        }
        public void onResults(Bundle results) {

            Log.d(TAG, "onResults " + results);
            // Fill the list view with the strings the recognizer thought it could have heard, there should be 5, based on the call
            ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            //display results.
            logthis("results: "+String.valueOf(matches.size()));
            for (int i = 0; i < matches.size(); i++) {
                Log.d(TAG, "result " + matches.get(0));
                logthis("result " +i+":"+ matches.get(0));
                outcome = matches.get(0);
            }
            gotSTT = false;

        }
        public void onPartialResults(Bundle partialResults)
        {
            Log.d(TAG, "onPartialResults");
        }
        public void onEvent(int eventType, Bundle params)
        {
            Log.d(TAG, "onEvent " + eventType);
        }
    }

    public void logthis (String newinfo) {
        if (newinfo != "") {
            log.setText(newinfo);
        }
    }


}
